package io.serialization;

import java.io.Serializable;

public class UserChild extends User implements Serializable {
    private int childLevel;

    public int getChildLevel() {
        return childLevel;
    }

    public void setChildLevel(int childLevel) {
        this.childLevel = childLevel;
    }
}
