package io.serialization;

import java.io.Serializable;
import java.util.Objects;

public class Sword implements Serializable {
    private int level;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sword)) return false;
        Sword sword = (Sword) o;
        return level == sword.level;
    }

    @Override
    public int hashCode() {

        return Objects.hash(level);
    }

    @Override
    public String toString() {
        return "Sword{" +
                "level=" + level +
                '}';
    }
}
