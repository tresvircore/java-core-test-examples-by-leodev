package io.serialization;

import java.io.*;

/**
 * Статические поля никогда не сериализуются!!!!
 * Transient поля НЕ ДОЛЖНЫ УЧАСТВОВАТЬ в equals hashcode, в нашем случае magicBook
 * Если родитель не имплементит Serializable, то код отработает без ошибок, но сохранит только наследника
 */
public class SerializationLesson {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        UserChild user = new UserChild();
//        User user = new User();
        user.setHealth(55);
        user.staticField = 45;
        Sword sword = new Sword();
        sword.setLevel(5);
        user.setSword(sword);
        user.setMagicBook(new MagicBook());
        user.setChildLevel(65);

        // Serialization
        FileOutputStream fos = new FileOutputStream("tempFile");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(user);
        oos.close();

        user.staticField = 35; // одно поле на все объекты

        // Deserialization
        FileInputStream fis = new FileInputStream("tempFile");
        ObjectInputStream ois = new ObjectInputStream(fis);
        User user2 = (User) ois.readObject();
        ois.close();
        // out
        System.out.println(user2.getHealth()); // 55
        System.out.println(user2.staticField); // 35
        System.out.println(user2.getSword().getLevel()); // 5
        System.out.println(user2.getMagicBook()); // null
        System.out.println(user.getChildLevel()); // 65
    }
}

