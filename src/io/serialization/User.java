package io.serialization;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private int health;
    private Sword sword;
    private transient MagicBook magicBook; // transient говорит пропустить поле при сериализации

    public static int staticField; // Статические поля никогда не сериализуются!!!!

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Sword getSword() {
        return sword;
    }

    public void setSword(Sword sword) {
        this.sword = sword;
    }

    public MagicBook getMagicBook() {
        return magicBook;
    }

    public void setMagicBook(MagicBook magicBook) {
        this.magicBook = magicBook;
    }

    // Transient поля НЕ ДОЛЖНЫ УЧАСТВОВАТЬ в equals hashcode
    // в нашем случае magicBook
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return health == user.health &&
                Objects.equals(sword, user.sword);
    }

    @Override
    public int hashCode() {

        return Objects.hash(health, sword);
    }

    @Override
    public String toString() {
        return "User{" +
                "health=" + health +
                ", sword=" + sword +
                ", magicBook=" + magicBook +
                '}';
    }
}
