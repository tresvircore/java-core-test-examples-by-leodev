package overloading_перегрузка;

// Перегразка методов.
// Играет роль только название метода, модификаторы и эксепшоны не играют никакой роли
public class OverloadingLesson {

    public static void main(String[] args) {
        OverloadingLesson overloadingLesson = new OverloadingLesson();
        int addToInt = overloadingLesson.addToDigit(5, 3);
        double addToDouble = OverloadingLesson.addToDigit(6.3, 3.3);

        System.out.println(addToInt);
        System.out.println(addToDouble);
    }

    public final int addToDigit(int a, int b) {
        return a + b;
    }

    protected static double addToDigit(double a, double b) {
        return a + b;
    }

}
