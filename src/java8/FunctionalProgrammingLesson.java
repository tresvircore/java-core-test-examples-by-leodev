package java8;

import java.util.function.Function;

public class FunctionalProgrammingLesson {

    public static void main(String[] args) {
        int square = square(5);
        int apply = squareFunc.apply(5);
        System.out.println(square);
        System.out.println(apply);
    }

    // java 7
    static int square(int x) {
        return x * x;
    }


    // java 8
    static Function<Integer, Integer> squareFunc = x -> x * x;

}
