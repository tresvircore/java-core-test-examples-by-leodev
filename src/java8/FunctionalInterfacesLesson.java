package java8;

import java.util.function.*;

/**
 * функциональные интерфейсы
 * <a href="https://www.youtube.com/watch?v=oUvx2Up-PkA&list=PL786bPIlqEjSAvjUJtG_q9bFxQnNvI0Ab&index=3">video</a>
 */
public class FunctionalInterfacesLesson {
    public static void main(String[] args) {
        // параметры: 1 на вход, 1 на выход
        Predicate<Integer> predicate = x -> x > 5; // сохраняем реализацию в переменную, но это обычный интерфейс
        System.out.println(predicate.test(3));
        System.out.println(predicate.test(7));

        // параметры: 1 на вход, void на выход
        Consumer<Integer> consumer = x -> System.out.println(x);
        Consumer<Integer> consumer2 = System.out::println;

        // параметры: 1 на вход, 1 на выход
        Function<Integer, String> function = x -> x.toString();
        Function<Integer, String> function2 = Object::toString;

        // на вход без параметров
        Supplier<Integer> supplier = () -> 5;
        Supplier<Integer> supplier2 = () -> new Integer(5);

        // входной и выходной параметр одиноковый
        UnaryOperator<Integer> operator = x -> x * x;

        BinaryOperator<Integer> binaryOperator = (x, y) -> x * y;


        // используем свой функциональный интерфейс
        MyPredicate<String> myPredicate = x -> System.out.println(x);
        myPredicate.apply("MyPredicate: " + 5);
    }
}

/**
 * Пишем свой функциональный интерфейс
 */
interface MyPredicate<T> {
    void apply(T t);
}
