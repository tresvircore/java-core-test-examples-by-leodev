package java8;

import java.util.concurrent.ForkJoinPool;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamsHowTheyWorksLesson {
    public static void main(String[] args) {
//        IntermediateExample();
//        consoleExample();
//        consoleExample2();
//        StreamException();
//        reusingStream();

        parallelStream();
    }



    /**
     * Intermidate - не работают сами по себе, в конце нужна терминальная операция(forEach, collect, reduce)
     */
    public static void IntermediateExample() {
        Stream<String> stream = Stream.of("a", "b", "c");
        stream.map(s -> {
            System.out.println("Map " + s);
            return s;
        });
    }

    // отработает все (вертикальный метод - каждый item по очереди проходит весь цикл операций)
    public static void consoleExample() {
        Stream<String> stream = Stream.of("a", "b", "c");
        stream.map(s -> {
            System.out.println("Map1 " + s);
            return s.toLowerCase();
        }).map(s -> {
            System.out.println("Map2 " + s);
            return s.toUpperCase();
        }).forEach(s -> System.out.println("ForEach " + s));
    }

    // сначала нужно фильтровать, а потом уже мапить. ForeEach не будет вызван ни разу
    private static void consoleExample2() {
        Stream<String> stream = Stream.of("a", "b", "c");
        stream.map(String::toUpperCase).filter(s -> {
            return s.equals("a");
        }).forEach(s -> System.out.println("ForEach " + s));
    }

    // стриму нельзя использовать 2 раз. Метод вызховерт Exception
    // java.lang.IllegalStateException: stream has already been operated upon or closed
    // из этой ситуации есть выход. Решение в методе reusingStream()
    private static void StreamException() {
        Stream<String> stream = Stream.of("a", "b", "c");
        stream.forEach(System.out::println);
        stream.forEach(System.out::println);
    }

    private static void reusingStream() {
        Stream<String> stream = Stream.of("a", "b", "c");
        Supplier<Stream<String>> supplier = () -> Stream.of("a", "b", "c"); // позволяет использовать данные несколько раз
        supplier.get().forEach(System.out::println);
        supplier.get().forEach(System.out::println);

    }

    /**
     * сортировка всегда выполняется в начале, где бы мы не написали ее
     *
     */
    private static void parallelStream() {
        Stream<String> stream = Stream.of("a", "b", "c");
        ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
        System.out.println(forkJoinPool.getParallelism()); // выводит колличество потоков 8 ядер, 7 потоков

        stream.parallel().sorted((s1, s2) -> {
            System.out.println("sort: " + s1 + " " + s2);
            return s1.compareTo(s2);
        }).map(s -> {
            System.out.println("Map: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map2: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map3: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map4: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map5: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map6: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map7: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map8: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map9: " + s + Thread.currentThread().getName());
            return s;
        }).map(s -> {
            System.out.println("Map10: " + s + Thread.currentThread().getName());
//            System.out.println(forkJoinPool.getPoolSize());
            return s;
        }).forEach(System.out::println);
    }
}
