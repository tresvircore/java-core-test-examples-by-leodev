package java8;

import java.util.stream.Stream;

/**
 * https://youtu.be/7b7_Pd2_iAM?list=PL786bPIlqEjSAvjUJtG_q9bFxQnNvI0Ab
 * MethodReference - ссылка на метод. Student::getName
 */
public class MethodReference {
    public static void main(String[] args) {
        Stream.of("one", "two").map(x -> x.toUpperCase());
        Stream.of("one", "two").map(String::toUpperCase);

        Stream.of(new Student("Leo"), new Student("Jack"))
                .map(Student::getName).forEach(System.out::println);

        // одинаковые методы. Создаем студентов
        Stream.of("Jhon", "Andy").map(Student::new).forEach(x -> System.out.println(x.getName()));
        Stream.of("Jhon", "Andy").map(x -> new Student(x)).forEach(x -> System.out.println(x.getName()));
    }
}

class Student{
    String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
