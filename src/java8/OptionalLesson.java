package java8;

import java.util.Optional;

/**
 * <a href="https://youtu.be/k7PlG32BzI8?list=PL786bPIlqEjSAvjUJtG_q9bFxQnNvI0Ab">Optional</a>
 */
public class OptionalLesson {
    public static void main(String[] args) {
        // если нужно засунуть NULL, то только так
//        Optional<String> optional1 = Optional.empty();
//        Optional<String> optional2 = Optional.ofNullable(null);


        Optional<String> optional = getString();
        if (optional.isPresent()) { // isPresent проверяет есть ли значение или
            System.out.println(optional.get());
        }
    }

    private static Optional<String> getString() {
        if(false){ // true или false
            return Optional.of("Hello"); // возвращаемое значение
        }
        return Optional.empty(); // замена null
    }
}
