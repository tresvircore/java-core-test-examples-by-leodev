package java8;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class CollectorLesson {
    public static void main(String[] args) {
        Stream.of("one", "two").collect(toList());
        Stream.of("one", "two").collect(toCollection(ArrayList::new));

        Stream.of("one", "two").collect(toSet());
        Stream.of("one", "two").collect(toCollection(HashSet::new));

        Stream.of("one", "two").collect(maxBy(Comparator.comparing(x -> x)));
        Stream.of(1, 2).collect(averagingInt(x -> x));

        // делит на 2 листа. false и true поделив элементы согласно условию
        Map<Boolean, List<Integer>> map = Stream.of(1, 2, 3, 4).collect(partitioningBy(x -> x > 2));

        // группировка как в sql
        Map<Integer, List<Integer>> map2 = Stream.of(1, 2, 1, 4).collect(groupingBy(x -> x));

        // joining дописывает символы в наале, в конце и разделитель между элементами. Удобно формировать JSON
        String res = Stream.of("one", "two", "three_Inheritance").collect(joining(",", "{", "}"));
        System.out.println(res);
    }
}
