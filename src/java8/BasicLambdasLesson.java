package java8;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Основы Лямбда выраений
 * https://www.youtube.com/watch?v=jHStUYP1NEg&list=PL786bPIlqEjSAvjUJtG_q9bFxQnNvI0Ab&index=2
 */
public class BasicLambdasLesson {
    public static void main(String[] args) {

    }
    /**
     * простейшая лямбда java 8
     */
    // сигнатура метода без типов -> тело метода
//         x -> System.out.println("Hello");
//        (x) -> System.out.println("Hello");
//        (x, y) -> System.out.println(x + y);
//        (x, y) -> { System.out.println(x + y) };


    //в java 7
    void someMethos(int x) {
        System.out.println("Hello");
    }

    /**
     * Анонимные классы
     */
    // java 7
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            System.out.println("Hello");
        }
    };

    ActionListener actionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Hello");
        }
    };

    // JAVA 8
    Runnable runnable_1 = () -> System.out.println("Hello");
    Runnable runnable_2 = () -> {
        System.out.println("Hello");
    };
    ActionListener actionListener_1 = event -> System.out.println("Hello");
    ActionListener actionListener_2 = event -> System.out.println("Hello");
    ActionListener actionListener_3 = event -> {
        System.out.println("Hello");
    };
    ActionListener actionListener_4 = event -> System.out.println(event.paramString());
    ActionListener actionListener_5 = (event) -> System.out.println(event.paramString());


    public static void someMethod() {
        // передаваемые параметры до 8 JAVA НУЖНО было писать FINAL в JAVA 8 она final по умолчанию для lambda
        String s = "Hello";
//        s = "new val"; // раскоментировать пример с ошибкой
        ActionListener al1 = event -> System.out.println(s);


    }

}
