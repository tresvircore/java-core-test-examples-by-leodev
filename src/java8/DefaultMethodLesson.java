package java8;

public class DefaultMethodLesson {
    public static void main(String[] args) {
        A a = new B();
        a.sayHello();


    }
}

interface A {
    // статический метод, потому что можем их создавать
    static  int add(int x, int y){
        return x + y;
    }

    void printMessage(String message);

    // default method
    default void sayHello(){
        System.out.println("A");
    }
}

interface C { // C extends A
    default void sayHello(){
        System.out.println("C");
    }
}

class B implements A{

    /**
     * Переопределить обязаны согласно контракта
     */
    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }

    /**
     * Переопределять default методы не обязательно, но мы моежм если хотим
     */
//    @Override
//    public void sayHello() {
//        System.out.println("new Hello");
//    }

    /**
     * ERROR Diamond problem: class D inherits unrelated defaults for sayHello() from types A and C
     * 1. Как решение мы сами можем переопределить метод sayHello()
     * 2. Или сделать C extends A
     */
    class D implements A, C{
        @Override
        public void printMessage(String message) {
            System.out.println(message);
        }

        // Как решение мы сами можем переопределить метод sayHello()
    @Override
    public void sayHello() {
        System.out.println("D");
    }
    }
}
