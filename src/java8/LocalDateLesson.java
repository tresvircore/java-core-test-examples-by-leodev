package java8;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateLesson {
    public static void main(String[] args) {
        LocalDateTime localDate = LocalDateTime.now();
        localDate.plusDays(10); // прибавляем дни
        localDate.minusMonths(1); // вычитаем месяц

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MM yyyy HH:mm");
        String res = localDate.format(formatter); // форматтер скармливаем дате
        System.out.println(res);

        LocalDate date = LocalDate.parse("01 02 2222 12:00", formatter);
        System.out.println(date);
    }

}
