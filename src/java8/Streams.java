package java8;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class Streams {
    public static void main(String[] args) {
        // demo data
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");

//        iteration(list);
//        streamIllegalStateExceptionExample(list);
//        FilterExample(list);
//        CollectorsExample(list);
//        MapExample(list);
//        FlatMapExample();
//        MinMaxExample();
//        reduceExample();

    }

    static void iteration(List<String> list) {
        // полная запись
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        // сокращенная запись
        for (String s : list) {
            System.out.println(s);
        }
        // java 8 lambda
        Stream<String> stream = list.stream();
        stream.forEach(x -> System.out.println(x));
        // аналогичная запись в JAVA 8
        list.stream().forEach(x -> System.out.println(x));
        // и еще короче
        list.forEach(System.out::println);
    }

    /**
     * Stream один раз открывается, и после закрывается. 2 раз его использовать нельзя. Каждый раз надо поучать новый
     *
     * @param list
     */
    static void streamIllegalStateExceptionExample(List<String> list) {
        // Stream один раз открывается, и после закрывается. 2 раз его использовать нельзя. Каждый раз надо поучать новый
        Stream<String> stream = list.stream();
        stream.forEach(x -> System.out.println(x));
//        stream.forEach(x -> System.out.println(x)); // Exception in thread "main" java.lang.IllegalStateException: stream has already been operated upon or closed
    }

    /**
     * filter - LAZY, выполняется только после вызова EAGER методов. Например count()
     */
    static void filterExample(List<String> list) {
        list.stream().filter(x -> x.equals("one"));

        // Если более 1 строки нужны скобки
        long res = list.stream().filter(x -> {
            System.out.println(x); // выведет one two
            return x.equals("one"); // отфильтрует только one
        }).count();

        // filter это LAZY оператор - не будет вызван
        Stream<String> stream = list.stream();
        stream.filter(x -> {
            System.out.println(x); // не будет вызван SOUT
            return x.equals("one");
        });
        // forEach это EAGER оператор
        stream.forEach(System.out::println);

    }

    /**
     * Collectors помогает собрать результирующие данные из стрима в коллекцию
     */
    static void collectorsExample(List<String> list) {
        Set<String> set = list.stream().collect(Collectors.toSet());
        // аналогичная запись
        Set<String> set2 = new HashSet<>(list);
        // фильтруем и возвращаем SET
        Set<String> setAfterFilter = list.stream().filter(x -> x.equals("one")).collect(Collectors.toSet());
    }

    /**
     * Map трансформирует объекты
     */
    static void mapExample(List<String> list) {
        list.stream().map(x -> x.toUpperCase()); // переводит строки в верхний регистр
    }

    /**
     * Map трансформирует объекты. ВСЕГДА ЗАКРЫВАЕТ ПОТОК!!!!
     * { {1,2}, {3,4}, {5,6} } -> flatMap -> {1,2,3,4,5,6}
     * { {'a','b'}, {'c','d'}, {'e','f'} } -> flatMap -> {'a','b','c','d','e','f'}
     */
    static void flatMapExample() {
        Stream<String> stream = Stream.of("one", "one"); // получаем стрим их массива
        Stream.of(                                       // получаем стрим из массива массивов
                Arrays.asList("three_Inheritance", "four"),
                Arrays.asList("five", "six")
        ).flatMap(x -> x.stream());
    }

    static void minMaxExample() {
        int valMin = Stream.of(1, 3, 2).min(Comparator.comparing(x -> x)).get();
        int valMax = Stream.of(1, 3, 2).max(Comparator.comparing(x -> x)).get();
        System.out.println("valMin: " + valMin + ", valMax: " + valMax);
    }

    /**
     * reduce складывает(аккумулирует) все элементы
     */
    static void reduceExample() {
        int count = Stream.of(1, 3, 2).reduce(0, (acc, element) -> acc + element);
        System.out.println(count); // 6

        // основан на BinaryOperator, пример выполнения
        BinaryOperator<Integer> accumulator = (acc, element) -> acc + element;
        int count2 = accumulator.apply(
                accumulator.apply(
                        accumulator.apply(0, 1),
                        2),
                3);
        System.out.println(count2); // 6
    }

    /**
     * Сортировка. Как обычно для своих сущностей нужен компаратор
     */
    static void sortedExample() {
        Stream.of("one", "three_Inheritance", "two").sorted().collect(Collectors.toList());
    }

    /**
     * аналогично SQL Limit ограничивает кол-во в результате
     */
    static void limitExample() {
        Stream.of("one", "three_Inheritance", "two").limit(2).collect(Collectors.toList()); // вернет только 2 значения
    }

    /**
     * убирает дубликаты в результате
     */
    static void distinctExample() {
        Stream.of("one", "one", "two").distinct().collect(Collectors.toList()); // вернет только уникальные значения
    }

    /**
     * skip - сколько значений пропустить
     */
    static void skipExample() {
        Stream.of("one", "one", "two").skip(1).collect(Collectors.toList()); //
    }

    /**
     * Находит любой или ервый элемент. Возвращают Optional(можно проверить есть результат или нет)
     */
    static void findAnyExample() {
        Stream.of("one", "three_Inheritance", "two").filter(x -> x.equals("two")).findAny().get();
        Stream.of("one", "three_Inheritance", "two").filter(x -> x.equals("two")).findFirst().get();
    }

    /**
     * mapToInt - превращает все в примитив int(4 байта), работать с int быстрее чем Integer (16 байт)
     */
    static void mapToIntExample() {
        IntStream intStream = Stream.of(1, 2, 3).mapToInt(x -> x);
        // intStream.summaryStatistics() - дает кучу методов для работы с цифрами
        intStream.summaryStatistics().getAverage();
    }

    /**
     * peek по функционалу похож намапу, надо гуглить
     */
    static void peekToIntExample() {
        Stream.of("one", "three_Inheritance", "two").peek(x -> x.toUpperCase());
    }
}


