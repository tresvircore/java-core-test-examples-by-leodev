package java8;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamParallelismLesson {
    public static void main(String[] args) {
        List<Integer> integers = populateListIntegers();
        checkParallelStreamVSStream(integers);
    }

    public static void checkParallelStreamVSStream(List<Integer> integers) {
        long start = System.nanoTime();
        List<Integer> list = integers.stream().sorted().collect(Collectors.toList());
        System.out.println("Stream: " + ((double)(System.nanoTime() - start) / 1_000_000.0) + " milisec");

        long start2 = System.nanoTime();
        List<Integer> list2 = integers.parallelStream().sorted().collect(Collectors.toList());
        System.out.println("ParallelStream: " + ((double)(System.nanoTime() - start2) / 1_000_000.0) + " milisec");
    }

    public static List<Integer> populateListIntegers() {
//        long start = System.nanoTime();
        int counter = 10_000_000;
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < counter; i++) {
            integers.add(i);
            integers.add(counter - i);
        }
//        System.out.println(System.nanoTime() - start) / 1000000);
        return integers;
    }

    static void createParallelStream() {
        // вызов стрима на разных ядрах процессора
        Stream stream = Stream.of(1, 2).parallel();
        // аналогично примеру выше
        List list = new ArrayList<>();
        Stream stream2 = list.parallelStream();
    }

    public static List<Integer> populateListIntegers2() {
//        long start = System.nanoTime();

        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 20_000_000; i++) {
            integers.add(i);
        }

//        long finish = System.nanoTime();
//        System.out.println((finish - start) / 1_000_000);
        return integers;
    }

}
