package generics;

import java.util.ArrayList;
import java.util.List;

class Parent {
    private String s = "some Parent text";

    public String getS() {
        return s;
    }
}

class Child extends Parent {
}

class Son extends Child {

}

/**
 * Ограничения:
 * - Джинерики не могут быть статическими
 * - Нельзя создать array джинериков
 * - Parent и Child - это разные типы, даже если один наследует другого
 * <p>
 * SUPER - идет вверх по иерархии, можем добавлять элементы ниже по иерархии
 * EXTEND - идет вниз по иерархии, не можем добавлять новые элементы
 */
public class GenericsLesson {
    public static void main(String[] args) {
        useGenericClass();
        incompatibleTypesExample();
        InheritGenerics();


    }

    private static void InheritGenerics() {
        List<Child> childList = new ArrayList<>();
        childList.add(new Child());
        childList.add(new Child());

        methodExtends(childList);
        methodSuper(childList);

        List<Parent> parentList = new ArrayList<>();
        parentList.add(new Parent());
        parentList.add(new Parent());

        methodSuper(parentList);
    }

    /**
     * Если EXTEND:
     * Метод принимает Parent и всех его наследников
     * Если мы пишем extends в дженериках, мы не можем добавить новые элементы.
     * Можно только читать!!!!
     */
    static void methodExtends(List<? extends Parent> list) {
        for (Parent parent : list)
            System.out.println("methodExtends: " + parent);

//        list.add(new Child());    // add (capture<? extends generics.Parent>) in List cannot be applied to (generics.Child)
        System.out.println("======");
    }

    /**
     * Если SUPER:
     * примет всех начиная с Child вплоть до Object
     */
    static void methodSuper(List<? super Child> list) {
        for (Object o : list) {
            Parent p = (Parent) o;
            System.out.println("methodSuper: " + p.getS());
        }
        // можем добавлять элементы
        list.add(new Son());
        list.add(new Child());
//        list.add(new Parent()); // не выше дженерализированного типа -> Child
        System.out.println("======");
    }


    private static void useGenericClass() {
        GenericsTest<String> genericsTest = new GenericsTest<>();
        genericsTest.<Integer>method(10);
        genericsTest.method2("some text var");
    }

    /***
     * Parent и Child - это разные типы, даже если один наследует другого
     * List<Parent> parentList = new ArrayList<Child>(); // IncompatibleTypes
     */
    public static void incompatibleTypesExample() {
//        List<Parent> parentList = new ArrayList<Child>();
    }
}

class GenericsTest<T> {
    T var;

    // нужно перечислить все типы участвующие в методе, только потом можно будет их использовать в методе
    <U> U method(U type) {
        System.out.println(type);
        return type;
    }

    // указывать используемые типы не обязательно если класс дженерализирован этим типом <T>
    T method2(T var) {
        System.out.println(var);
        this.var = var;
        return var;
    }

    <U, J> U methodWithManyParams(U var, J var2) {
        // some logic
        return var;
    }
}
