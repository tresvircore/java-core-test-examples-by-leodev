package collections;

import java.util.*;

public class SetLesson {
    public static void main(String[] args) {
//        HashSetExample();
//        differentSets();
        customEntity();

        Set<String> hashSet = new HashSet<>();
        Set<String> linkedHashSet = new LinkedHashSet<>();
        Set<String> treeSet = new TreeSet<>();


    }

    // HashSet - использует hashcode для добавления элементов
    // TreeSet - использует компаратор для добавления элементов [Обязательно implements COMPARABLE]
    private static void customEntity() {
        Set<Student> hashSet = new HashSet<>();
        Set<Student> treeSet = new TreeSet<>();
        System.out.println("===HashSet<Student>");
        hashSet.add(new Student(3));
        hashSet.add(new Student(1));
        hashSet.add(new Student(2));
        for (Student st : hashSet) {
            System.out.println(st.getId());
        }

        System.out.println("===TreeSet<Student>");
        treeSet.add(new Student(3));
        treeSet.add(new Student(1));
        treeSet.add(new Student(2));
        for (Student st : treeSet) {
            System.out.println(st.getId());
        }
    }

    // HashSet не упорядоченый список
    // LinkedHashSet связный список элементов. Сохраняется порядок вставки
    // TreeSet - отсортированный список, в данном случае по алфавиту. Наследник NavigableMap
    public static void differentSets() {
        Set<String> hashSet = new HashSet<>();
        Set<String> linkedHashSet = new LinkedHashSet<>();
        Set<String> treeSet = new TreeSet<>();

        System.out.println("== HashSet");
        hashSet.add("five");
        hashSet.add("four");
        hashSet.add("three_Inheritance");
        hashSet.add("two");
        hashSet.add("one");
        for (String s : hashSet) {
            System.out.println(s);
        }

        System.out.println("== LinkedHashSet");
        linkedHashSet.add("five");
        linkedHashSet.add("four");
        linkedHashSet.add("three_Inheritance");
        linkedHashSet.add("two");
        linkedHashSet.add("one");
        for (String s : linkedHashSet) {
            System.out.println(s);
        }

        System.out.println("== TreeSet");
        treeSet.add("five");  // 1
        treeSet.add("four");  // 2
        treeSet.add("three_Inheritance"); // 4
        treeSet.add("two");   // 5
        treeSet.add("one");   // 3
        for (String s : treeSet) {
            System.out.println(s);
        }
    }

    // Только уникальные значения, может хранить null
    public static void HashSetExample() {
        Set<String> hashSet = new HashSet<>();
        hashSet.add(null);
        hashSet.add("A");
        hashSet.add("A");
        System.out.println(hashSet.size()); // 2 null и  A
        System.out.println(hashSet.contains(null)); // true
    }


    static class Student implements Comparable{
        int id;

        public Student(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Student)) return false;
            Student student = (Student) o;
            System.out.println("equals()");
            return id == student.id;
        }

        @Override
        public int hashCode() {
            System.out.println("hashCode");
            return Objects.hash(id);
        }

        @Override
        public String toString() {
            return "Student{" +
                    "id=" + id +
                    '}';
        }

        @Override
        public int compareTo(Object o) {
            System.out.println("compareTo()");
            return id - ((Student)o).getId();
        }
    }
}
