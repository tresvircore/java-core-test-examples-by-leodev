package collections;

import java.util.*;

public class CollectionsLessons {
    public static void main(String[] args) {
//        collectionExample();
//        listExample();
//        setExample();
//        QueueExample();
        mapExample();

    }

    private static void mapExample() {
        Map<String, String> map = new HashMap<>();
        map.put("1", "one");
        map.put("2", "two");
        map.put("3", "three_Inheritance");
        map.put(null, "some null");
        System.out.println(map.get("1"));
        System.out.println(map.get("2"));
        System.out.println(map.get(null));

        map.remove("1");
        System.out.println(map.get("1"));
        System.out.println(map.get("2"));
        System.out.println(map.get("3"));
        System.out.println(map.get(null));
    }

    // LinkedList одна из имплементаций. FIFO / FILO
    public static void QueueExample() {
        Queue<String> queue = new LinkedList<>();
        queue.add("zero"); // offer()
        queue.offer("one"); // add()
        queue.add("two");

        System.out.println(queue.element());
        System.out.println(queue.element());
        System.out.println(queue.element());
    }

    // нет дубликатов, только уникальные значения
    public static void setExample() {
        Set<String> set = new HashSet<>();
        set.add("one");
        set.add("one");
        set.add("two");
        System.out.println(set.size());
    }

    // пронумерованый список
    public static void listExample() {
        List<String> list = new ArrayList<>();
        list.add("zero");
        list.add("one");
        list.add("two");
        System.out.println(list.get(0));
    }


    public static void collectionExample() {
        Collection<String> collection = new ArrayList<>();

        collection.add("one");
        collection.add("two");
        System.out.println(collection.size());

        collection.remove("one");
        System.out.println(collection.size());
        System.out.println(collection.contains("one"));

        Iterator<String> iterator = collection.iterator();
        while (iterator.hasNext())
            System.out.println(iterator.next());
    }
}
