package collections;

import java.util.*;

/**
 * обязательно переопределяем hashcode() и equals()
 * HashMap занимает больше памяти чем TreeMap так как у HashMap минимум 25% ячеек занимают NULL, основана на массиве
 */
public class MapLesson {
    private static HashMap<Object, Object> hashMap = new HashMap<>();
    private static LinkedHashMap<Object, Object> linkedHashMap = new LinkedHashMap<>();
    private static TreeMap<Object, Object> treeMap = new TreeMap<>(); // красно-черные деревья
    private static Hashtable<Object, Object> hashtable = new Hashtable<>();
    private static Map<Object, Object> synchronizedMap = Collections.synchronizedMap(new Hashtable<>());

    public static void main(String[] args) {
        treeMapExample();
    }

    public static void treeMapExample() {
        treeMap.put("a","1");
        treeMap.put("b","2");
        treeMap.put("c","3");
        treeMap.put("d","4");

        // получаем subMap()
        SortedMap<Object, Object> subMap = treeMap.subMap("a", "c"); // {a=1, b=2}. C не включительно
        SortedMap<Object, Object> subMap2 = treeMap.subMap("c", "d"); // {c=3}. D не включительно
        System.out.println(subMap);
        System.out.println(subMap2);
        System.out.println(subMap.firstKey()); // a - вернет первый КЛЮЧ
        System.out.println(subMap.lastKey()); // b - вернет последний КЛЮЧ

        // NavigableMap
        System.out.println("=== NavigableMap");
        System.out.println(treeMap.headMap("c")); // {a=1, b=2}
        System.out.println(treeMap.tailMap("c")); // {c=3, d=4}
        System.out.println(treeMap.headMap("c").lastKey()); // b


    }
}
