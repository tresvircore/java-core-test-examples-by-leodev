package collections;

import java.util.*;

public class ListsLesson {
    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();
        arrayList.add("A");
        arrayList.add("B");
        arrayList.add("C");
        for (int i = 0; i < arrayList.size(); i++) {
            String s = arrayList.get(i);
            System.out.println(s);
        }

    }

//    List<String> linkedList = new LinkedList<>();
//    List<String> vector = new Vector<>();
//    List<String> stack = new Stack<>();
//    List<String> syncList = Collections.synchronizedList(new ArrayList<>());
}
