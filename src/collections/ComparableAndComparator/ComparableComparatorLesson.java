package collections.ComparableAndComparator;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * <H2>Видео по примеру <a href="https://youtu.be/x4CUbW-K8E8">Урок по Java 51: Сортировка коллекций и интерфейсы - Comparable и Comparator</a> </H2>
 *
 * <p>Начиная с версии 1.5, в Java появились два интерфейса {@link java.lang.Comparable} и {@link java.util.Comparator}.</p>
 * <p>Объекты классов, реализующие один из этих интерфейсов, могут быть упорядоченными. Зачем же тогда два интерфейса, которые делают одно,
 * и тоже действие, спросите вы. Вот об этом мы и поговорим в этой статье.</p>
 *
 * <p>Интерфейс <B>Comparable</B> </p>
 * <p>В интерфейсе Comparable объявлен всего один метод compareTo(Object obj), предназначенный для реализации упорядочивания объектов класса. Его удобно использовать при сортировке упорядоченных списков или массивов объектов.
 * Данный метод сравнивает вызываемый объект с obj. В отличие от метода equals, который возвращает true или false, compareTo возвращает:</p>
 * <ul>
 * <li>0, если значения равны;</li>
 * <li>Отрицательное значение, если вызываемый объект меньше параметра;</li>
 * <li>Положительное значение ,  если вызываемый объект больше параметра.</li>
 * </ul>
 * <p>Метод может выбросить исключение ClassCastException, если типы объектов не совместимы при сравнении.</p>
 * <p>Необходимо помнить, что  аргумент метода compareTo имеет тип сравниваемого объекта класса.</p>
 * <p>Классы Byte, Short, Integer, Long, Double, Float, Character, String уже реализуют интерфейс Comparable.</p>
 */
public class ComparableComparatorLesson {
    public static void main(String[] args) {
        compareObject();
        compareCustomObjectExample1();
        compareCustomObjectExample2();

//        classCastExceptionExample();
    }

    public static void compareCustomObjectExample1() {
        Set<Person> personSet = new TreeSet<>();
        personSet.add(new Person(4));
        personSet.add(new Person(6));
        personSet.add(new Person(4));
        personSet.add(new Person(2));
        for (Person person : personSet) {
            System.out.println("compareCustomObjectExample1: " + person.getAge());
        }
        System.out.println("=========");
    }

    private static void compareCustomObjectExample2() {
        ComparePerson comparePerson = new ComparePerson();
        Set<PersonWithoutComparable> personSet = new TreeSet<>(comparePerson);

        personSet.add(new PersonWithoutComparable(4));
        personSet.add(new PersonWithoutComparable(6));
        personSet.add(new PersonWithoutComparable(4));
        personSet.add(new PersonWithoutComparable(2));
        for (PersonWithoutComparable p : personSet) {
            System.out.println("compareCustomObjectExample2: " + p.getAge());
        }
        System.out.println("=========");
    }

    // не знет как сортировать т.к. не реализован интерфейс comparable
    // java.lang.ClassCastException: collections.ComparableAndComparator.Person cannot be cast to java.base/java.lang.Comparable
    public static void classCastExceptionExample() {
        Set<PersonWithoutComparable> personSet = new TreeSet<>();
        personSet.add(new PersonWithoutComparable(4));
        personSet.add(new PersonWithoutComparable(6));
        personSet.add(new PersonWithoutComparable(4));
        personSet.add(new PersonWithoutComparable(2));
    }

    // сортирует строки по умолчанию
    public static void compareObject() {
        Set treeSet = new TreeSet();
        treeSet.add("2");
        treeSet.add("5");
        treeSet.add("4");
        treeSet.add("1");
        for (Object o : treeSet) {
            System.out.println("compareObject: " + o);
        }
        System.out.println("=========");
    }
}

class Person implements Comparable<Person>{
    private int age;

    public Person(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // 0 - равны,
    @Override
    public int compareTo(Person p) {
        return this.age - p.getAge();
    }
}

class PersonWithoutComparable {
    private int age;

    public PersonWithoutComparable(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

class ComparePerson implements Comparator<PersonWithoutComparable> {
    @Override
    public int compare(PersonWithoutComparable o1, PersonWithoutComparable o2) {
        return o1.getAge() - o2.getAge();
    }
}
