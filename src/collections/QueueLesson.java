package collections;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueLesson {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();
        Deque<String> deque = new LinkedList<>();
        Deque<String> arrayDeque = new ArrayDeque<>(); // очередь на основе массива
        Queue<String> priorityQueue = new PriorityQueue<>(); // сортирует элементы
        LinkedBlockingQueue<String> linkedBlockingQueue = new LinkedBlockingQueue<>(); // при заполнении блокирует добавление пока кто-то не заберет объект из очереди освободив место

        System.out.println("arrayDeque");
        arrayDeque.add("A");
        arrayDeque.add("C");
        arrayDeque.add("B");
        for (String s : arrayDeque) {
            System.out.println(s);
        }

        // возвращает не отсотрированный
        // при удалении возвращает отсортированный
        System.out.println("priorityQueue");
        priorityQueue.add("A");
        priorityQueue.add("C");
        priorityQueue.add("B");
        for (String s : arrayDeque) {
            System.out.println(s);
        }

        System.out.println("priorityQueue.куьщму()   - SORT");
        //sorted by remove
        while (priorityQueue.size() != 0)
            System.out.println(priorityQueue.remove());

    }

}
