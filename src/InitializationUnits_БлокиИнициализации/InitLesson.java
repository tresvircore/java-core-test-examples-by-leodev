package InitializationUnits_БлокиИнициализации;


class Parent {
    static {
        System.out.println("PARENT STATIC BLOCK"); // 1
    }

    public Parent() {
        System.out.println("Parent constructor"); // 3
    }
}

public class InitLesson extends Parent {

    static {
        System.out.println("STATIC BLOCK"); // 2
    }

    static {
        System.out.println("STATIC BLOCK 2"); // 2.2
    }

    {
        System.out.println("INIT BLOCK"); // 4
    }

    {
        System.out.println("INIT BLOCK 2"); // 4.2
    }

    public InitLesson() {
        System.out.println("CONSTRUCTOR"); // 5
    }

    public static void main(String[] args) {
        new InitLesson();
    }

}
