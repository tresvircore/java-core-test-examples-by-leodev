package proxy;

import io.serialization.User;

import java.lang.reflect.Proxy;

/**
 * https://leodev.ru/blog/java/java-dynamic-proxy/
 */
public class DinamicProxyTest {
    public static void main(String[] args) {
        User user = new User();
        IUser userProxy = (IUser) Proxy.newProxyInstance(
                User.class.getClassLoader(),
                User.class.getInterfaces(),
                new UserInvocationHandler(user)
        );
        userProxy.setName("Гриша");
        final String name = userProxy.getName();
        userProxy.rename("Вася");
    }
}
