package proxy;

public interface IUser {
    String getName();
    void setName(String name);
    void rename(String newName);
}
