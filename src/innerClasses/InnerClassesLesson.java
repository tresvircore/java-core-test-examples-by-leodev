package innerClasses;

/**
 * - Без экземпляра внешнего класса мы не сможем создать экземпляр внутреннего
 * -
 */
public class InnerClassesLesson { // внешний класс
    private int i = 5;

    public static void main(String[] args) {
        InnerClassesLesson innerClassesLesson = new InnerClassesLesson();
        innerClassesLesson.method();
    }

    static class staticClass {
//      i = 5; // не имеет доступа к переменной i так как статический класс инициализируется до создания объектов
    }

    void method() {
        Inner inner = new Inner();
        inner.method2();
        System.out.println(inner.k); // имеет доступ к своим полям, что логично
    }

    // имеет полный доступ ко всем полям внешнего класса!!!
    class Inner { // внутренний класс
        private int k = 6;

        void method2() {
            System.out.println(i); // имеет доступ к полям внешнего класса
        }
    }
}
