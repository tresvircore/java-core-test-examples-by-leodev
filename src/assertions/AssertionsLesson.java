package assertions;

/**
 * По умолчанию assert отключены
 * enable assertions -> активирует команда: -ea
 */
public class AssertionsLesson {
    public static void main(String[] args) {
        method(-5);
        method(3);
    }

    static void method(int a) {
        assert a > 0; // если false ->
        System.out.println(a);
    }
}
