package accessModifiers.one;

public class TestClazz {
    private int i = 5; // виден только внутри класса (TestClazz)
    int k = 7;           // package / default - виден на уровне пакета
    protected int j = 6; // package - виден на уровне пакета и наследникам
    public int m = 8; // виден везде

    // inner class видит все
    class TestModifiers{
        void method(){
            TestClazz testClazz = new TestClazz();
            System.out.println(testClazz.j);
            System.out.println(testClazz.k);
            System.out.println(testClazz.m);
            System.out.println(testClazz.i);
        }
    }

}
// outer class все кроме private так как в одном пакете
class TestModifiers{
    void method(){
        TestClazz testClazz = new TestClazz();
        System.out.println(testClazz.j);
        System.out.println(testClazz.k);
        System.out.println(testClazz.m);
    }
}
