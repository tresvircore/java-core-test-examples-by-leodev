package accessModifiers.three_Inheritance;

import accessModifiers.one.TestClazz;
// наследник видит public и protected
public class TestModifiers extends TestClazz{
    void method(){
        TestClazz testClazz = new TestClazz();
        System.out.println(testClazz.m); // public
        j = 9; // protected
    }
}
