package accessModifiers.two;

import accessModifiers.one.TestClazz;

// только public так как в другом пакете и не наследует
public class TestModifiers {
    void method(){
        TestClazz testClazz = new TestClazz();
        System.out.println(testClazz.m); // public
    }
}
