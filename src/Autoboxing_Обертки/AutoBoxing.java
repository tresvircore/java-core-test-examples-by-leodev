package Autoboxing_Обертки;

import java.util.Objects;

/**
 * Все объекты и обертки в том числе сравниваем через equals
 * Примитивы через ==
 * Integer Pool
 */
public class AutoBoxing {
    public static void main(String[] args) {
        equalsExample();

    }

    public static void equalsExample() {
        Integer a = 5;
        Integer b = new Integer(5);
        System.out.println(">>> a vs b");
        System.out.println(a == b); // false
        System.out.println(a.equals(b)); // true
        System.out.println(Objects.equals(a, b)); // true

        Integer c = 127;
        Integer d = 127;
        System.out.println(">>> c vs d");
        System.out.println(c == d); // true
        System.out.println(c.equals(d)); // true
        System.out.println(Objects.equals(c, d)); // true

        Integer x = 128;
        Integer y = 128;
        System.out.println(">>> x vs y");
        System.out.println(x == y); // false
        System.out.println(x.equals(y)); // true
        System.out.println(Objects.equals(x, y)); // true
    }


    static void incrementNotAtomicOperation() {
        Integer i = new Integer(5);
        i++;
        System.out.println(i);

        // так это делает компилятор
//        Integer i = new Integer(5);
//        i = i + 1;
//        System.out.println(i);

    }

    void Example() {
        // 8 примитивных типов
        int i = 5;
        Integer integer;

        byte b = 1;
        Byte bt;

        short s;
        Short shrt;

        long l;
        Long lng;

        boolean bl;
        Boolean bln;

        char ch;
        Character chr;

        float f;
        Float fl;

        double d;
        Double doubl;
    }
}
