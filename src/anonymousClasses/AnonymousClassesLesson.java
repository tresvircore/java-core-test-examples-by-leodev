package anonymousClasses;

class Popcorn{
    void doSome(){
        System.out.println("Popcorn");
    }

    void secondMethod(){
        System.out.println("second");
    }
}

public class AnonymousClassesLesson {
    public static void main(String[] args) {
        createAnonymousClassed();
        createAnonymousComparable();

    }

    public static void createAnonymousClassed() {
        // new Popcorn() - создает анонимный класс-наследник и переопределяем метод doSome()
        Popcorn popcorn = new Popcorn() {
            @Override
            void doSome() {
                System.out.println("Main");
            }
        };

        popcorn.doSome();
        popcorn.secondMethod();
    }

    // так же можем имплементить интерфейсы и сразу писать реализацию
    public static void createAnonymousComparable(){
        // java 7
        Comparable comparable = new Comparable() {
            @Override
            public int compareTo(Object o) {
                return 0;
            }
        };

        // java 8
        Comparable comparable2 = o -> 0;

        // вызываем сам метод
        comparable.compareTo(new Object());
    }
}
