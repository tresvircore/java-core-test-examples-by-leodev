package enumiration;

/**
 * Создавать можно как отсдельно так и в нутри класса. Нельзя создавать внутри методов
 * Может иметь конструктор @{{@link CoffeeSize}}
 *
 */
public class EnumExample {
    public static void main(String[] args) {
        CoffeeSize coffeeSizeMed = CoffeeSize.MEDIUM;
        System.out.println(coffeeSizeMed.name());
        System.out.println(coffeeSizeMed.getCoffeeClass());
        System.out.println(coffeeSizeMed.getMilliliters());

        CoffeeSize coffeeSizeBig = CoffeeSize.BIG;
        System.out.println(coffeeSizeBig.name());
        System.out.println(coffeeSizeBig.getCoffeeClass());
        System.out.println(coffeeSizeBig.getMilliliters());
    }

    // пример enum внутри класса
    enum CoffeeSize2 {
        SMALL,
        MEDIUM,
        BIG
    }



}

