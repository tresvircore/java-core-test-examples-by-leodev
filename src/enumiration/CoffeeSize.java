package enumiration;

// Может иметь конструктор
public enum CoffeeSize {
    SMALL(100),
    MEDIUM(200),
    BIG(300){
        @Override
        String getCoffeeClass() {
            return "B";
        }
    };

    int milliliters; // может хранить значение инициализируя через конструктор
    String coffeeClass = "A";

    CoffeeSize(int milliliters) {
        this.milliliters = milliliters;
    }

    String getCoffeeClass(){
        return coffeeClass;
    }

    public int getMilliliters() {
        return milliliters;
    }
}
