package EqualsAndHashCode;

import java.util.HashMap;
import java.util.Objects;

/**
 * http://www.quizful.net/interview/java/difference-comparator-from-comparable
 *
 * Interface Comparable задает свойство сравнения объекту реализующему его. То есть делает объект сравнимым (по правилам разработчика).
 * Interface Comparator позволяет создавать объекты, которые будут управлять процессом сравнения (например при сортировках).
 * А реальное отличие - в том что Comparator в отличие от Comparable можно использовать для сравнения final объектов сторонних библиотек не позволяющих заимплементить Comparable.
 * Comparable > Comparator
 *
 * В версии 5.0 java.lang.Boolean реализует интерфейс java.lang.Comparable, причем таким образом, что false<true.
 *
 * x.compareTo(y) возвращает -1 или 1, если x должен находиться, соответственно, раньше или позже y. Если метод возвращает 0, то порядки (x, y) и (y, x) эквивалентны.
 * Если sign(a) – функция, возвращающая -1,0,1 для а, соответственно, меньше 0, равного 0 и больше 0, то должно выполняться равенство sign(x.compareTo(y))==-sign(y.compareTo(x)). Что логично: если x идет раньше y, то y должен идти позже x, и наоборот.
 * Если x.compareTo(y) > 0 и y.compareTo(z) > 0, то x.compareTo(z) > 0 – соотношение транзитивности неравенств.
 * Если x.compareTo(y) == 0, то sign(x.compare(z)) == sign(y.compareTo(z)), для любых z.
 * Вызов x.compareTo(null) должен бросать исключение NullPointerException. В этом есть расхождение с логикой реализации equals (напомню, x.equals(null) возвращает false).
 * Если y по своему типу не может быть сравнен с x, то вызов x.compareTo(y) должен бросать исключение ClassCastException.
 * (x.compareTo(y) == 0) == x.equals(y), т.е. вызов x.compareTo(y) должен возвращать 0 тогда и только тогда, когда x.equals(y) возвращает true. Это правило непротиворечивости, и его очень важно учитывать.
 */
public class EqualsAndHashCodeLesson {
    public static void main(String[] args) {
        String author = "Pushkin";
        String name = "How to get friends";

        Book book = new Book(author, name);
        HashMap<String, Book> library = new HashMap<>();
        library.put(name, book);

        Book pushkinBook = library.get(name);
        System.out.println(pushkinBook);
        System.out.println(pushkinBook.hashCode());
        System.out.println(new Book().hashCode());
    }
}

class Book{
    private String author;
    private String name;

    public Book() {
    }

    public Book(String author, String name) {
        this.author = author;
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return Objects.equals(author, book.author) &&
                Objects.equals(name, book.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(author, name);
    }
}
