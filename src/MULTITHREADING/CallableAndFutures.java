package MULTITHREADING;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Callable подобен Runnable, но с возвратом значения.
 * Интерфейс Callable является параметризованным типом, с единственным общедоступным методом call()
 * =================================
 * Future хранит результат асинхронного вычисления.
 * Вы можете запустить вычисление, предоставив кому-либо объект Future, и забыть о нем.
 * Владелец объекта Future может получить результат, когда он будет готов.
 *
 * Вызов первого метода get() устанавливает блокировку до тех пор, пока не завершится вычисление.
 * Второй метод генерирует исключение TimeoutException, если истекает таймаут до завершения вычислений.
 * Если прерывается поток, выполняющий вычисление, оба метода генерируют исключение InterruptedException.
 * Если вычисление уже завершено, get() немедленно возвращает управление.
 *
 * isDone() возвращает false, если вычисление продолжается, и true - если оно завершено.
 * cancel() прерывает вычисление
 *      Если вычисление еще не стартовало, оно отменяется и уже не будет запущено.
 *      Если же вычисление уже идет, оно прерывается в случае равенства true параметра mayInterrupt.
 * =================================
 * FutureTask - Класс-оболочка, удобный механизм для превращения Callable одновременно в Future и Runnable, реализуя оба интерфейса.
 *
 * http://pro-java.ru/java-dlya-opytnyx/interfejsy-callable-i-future-v-java/
 * https://youtu.be/SWKvCA8eClg?list=PL786bPIlqEjRDXpAKYbzpdTaOYsWyjtCX
 */
public class CallableAndFutures {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Integer> callable = new MyCallable();
        FutureTask futureTask = new FutureTask<>(callable);

        Thread thread = new Thread(futureTask);
        thread.start();

        System.out.println(futureTask.get()); // futureTask.get() зависнет и будет ждать пока не завершится работа MyCallable
    }

    static class MyCallable implements Callable<Integer> {
        @Override
        public Integer call() throws InterruptedException {
            int x = 0;
            for (int i = 0; i < 10; i++, x++) {
                Thread.sleep(300);
            }
            return x;
        }
    }
}
