package MULTITHREADING;

import java.util.concurrent.*;

/**
 * https://tproger.ru/translations/java8-concurrency-tutorial-1/ - Параллельное выполнение кода с помощью потоков
 * https://youtu.be/vVyjcJLFNWQ?list=PL786bPIlqEjRDXpAKYbzpdTaOYsWyjtCX
 * https://habr.com/post/116363/
 * https://habr.com/post/260953/ - 10 советов по использованию ExecutorService
 *
 * Работу исполнителей надо завершать явно. Для этого в интерфейсе ExecutorService есть два метода:
 *      shutdown(), который ждет завершения запущенных задач
 *      shutdownNow(), который останавливает исполнитель немедленно
 *
 *
 */
public class ExecutorsLesson {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(() -> System.out.println(1)); // Runnable print 1
        Future<String> future = service.submit(() -> "2");// Callable return "2"
        System.out.println(future.get());
        service.shutdown(); //  Работу исполнителей надо завершать явно.

        // ExecutorService
        createExecutorService();
        newCachedThreadPoolExample();

        // ScheduledExecutorService
        startWaiting5Sec();
        repeatEverySecond();
    }



    // ===================== ExecutorService
    /**
     * Допустим, надо запустить какой-нибудь код асинхронно 10 раз.
     * <p>
     * Метод submit также возвращает объект Future, который содержит информацию о статусе исполнения
     * переданного Runnable или Callable (который может возвращать значение).
     * Из него можно узнать выполнился ли переданный код успешно, или он еще выполняется.
     * Вызов метода get на объекте Future возвратит значение, который возвращает Callable (или null, если используется Runnable). М
     * етод имеет 2 checked-исключения:
     * InterruptedException, который бросается, когда выполнение прервано через метод interrupt()
     * ExecutionException если код в Runnable или Callable бросил RuntimeException
     * что решает проблему поддержки исключений между потоками.
     */
    public static void newCachedThreadPoolExample() {
        ExecutorService service = Executors.newCachedThreadPool();
        for (int i = 0; i < 10; i++) {
            service.submit(new Runnable() {
                public void run() {
                    System.out.println(Thread.currentThread().getName() + " newCachedThreadPoolExample()");
                }
            });
        }
    }

    public static void createExecutorService() {
        // создает пул с 1 потоком
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

        // ExecutorService исполняет асинхронный код в одном или нескольких потоках. Создание инстанса ExecutorService'а
        // делается либо вручную через конкретные имплементации (ScheduledThreadPoolExecutor или ThreadPoolExecutor),
        // но проще будет использовать фабрики класса Executors.
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(5); // создает пул с заданным колличеством потоков

        // пул потоков, который создает потоки по мере необходимости, но переиспользует неактивные потоки (и подчищает потоки,
        // которые были неактивные некоторое время)
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    }


    // ===================== ScheduledExecutorService
    /**
     * ScheduledExecutorService - позволяет поставить код выполняться в одном или нескольких потоках и сконфигурировать интервал или время,
     * на которое выполненение будет отложено. Интервалом может быть время между двумя последовательными запусками
     * или время между окончанием одного выполнения и началом другого. Методы ScheduledExecutorService возвращают ScheduledFuture,
     * который также содержит значение отсрочки для выполнения ScheduledFuture.
     */

    //отложить выполнение на 5 секунд
    public static void startWaiting5Sec() {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("Текст через 5 сек");
            }
        }, 5, TimeUnit.SECONDS);
        service.shutdown();
    }

    // повторять выполнение каждую секунду
    public static void repeatEverySecond() {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                System.out.println("Текст выводится каждую секунду");
            }
        }, 0, 1, TimeUnit.SECONDS);
        service.shutdown();
    }


}
