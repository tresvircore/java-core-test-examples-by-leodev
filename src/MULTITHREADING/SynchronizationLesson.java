package MULTITHREADING;

public class SynchronizationLesson {
    public static void main(String[] args) throws InterruptedException {
        Resource resource = new Resource();
        resource.setI(5);
        resource.setX(10);
        Resource.setZ(15);


        Thread t1 = new Thread(new MyRunnable2(resource), "t1");
        Thread t2 = new Thread(new MyRunnable2(resource), "t2");
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("Result is I: " + resource.getI() + ", X: " + resource.getX() + ", Z: " + Resource.getZ());
    }
}

class MyRunnable2 implements Runnable {
    Resource resource;

    public MyRunnable2(Resource resource) {
        this.resource = resource;
    }


    @Override
    public void run() {
        resource.changeI();
        resource.changeX();
        resource.changeStaticZ();
    }
}

class Resource {
    private int i;
    private int x;
    private static int z;

    public int getI() {
        return i;
    }

    public synchronized void setI(int i) {
        this.i = i;
    }

    public int getX() {
        return x;
    }

    public synchronized void setX(int x) {
        this.x = x;
    }

    public static int getZ() {
        return z;
    }

    public static void setZ(int z) {
        Resource.z = z;
    }

    public synchronized void changeI() { // synchronized не дает выполнять метод пока не закорнчит работу другой поток
        int i = this.i;
        System.out.println(Thread.currentThread().getName() + ", I: " + i);
        i++;
        System.out.println(Thread.currentThread().getName() + ", I: " + i);
        this.i = i;
    }

    public void changeX() {
        synchronized (this) { // synchronized блок не дает выполнять метод пока не закорнчит работу другой поток, для лока использует объект класса
            int x = this.x;
            System.out.println(Thread.currentThread().getName() + ", X: " + x);
            x++;
            System.out.println(Thread.currentThread().getName() + ", X: " + x);
            this.x = x;
        }
    }

    public synchronized void changeStaticZ() { // synchronized в статических классах блокирует весь клас целиком использщуя класс как монитор ( synchronized (Resource.class){...} )
        int z = Resource.z;
        System.out.println(Thread.currentThread().getName() + ", Z: " + z);
        z++;
        System.out.println(Thread.currentThread().getName() + ", Z: " + z);
        Resource.z = z;
    }
}