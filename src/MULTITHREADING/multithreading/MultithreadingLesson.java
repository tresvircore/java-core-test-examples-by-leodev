package MULTITHREADING.multithreading;

import MULTITHREADING.JoinExample;

import java.util.concurrent.*;

/**
 * http://javastudy.ru/interview/concurrent/
 *
 * Thread.start(); запускает дочерний поток. Для интерфейса Callable запуск потока осуществляется с помощью метода submit().
 * Thread.sleep(3000); усыпит текущий поток на 3 секунды
 * Thread.yield(); - меняет состояние потока из running в runnable (на усмотрение виртуальной машины может ничего не делать.
 *          Польза yield как и приоритетов весьма сомнительна,). прекращает своё выполднение и отдает время другому потоку
 * thread.interrupt(); - установит флаг interrupt и бросит InterruptedException()
 * thread.join();  {@link JoinExample} - прекращает выполнение текущего потока пока не закончит работать поток на котором вызвали join().
 * thread.setPriority(Thread.MAX_PRIORITY); - устанавливает приоритет потоков от 0 до 10
 * thread.wait(1000), заставляет заснуть текущий поток на одну секунду.  Поток может спать менее секунды, если получает notify() или notifyAll().
 *
 * СОСТОЯНИЯ ПОТОКА:
 *           Waiting/blocking
 *            ↓           ↑
 * NEW -> Runnable <-> Running -> Dead
 *
 * В Running находится всегда только 1 поток, остальные находятся в пуле
 */
public class MultithreadingLesson {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        createNewThreads(); // создаем потоки
//        sleepExample();
//        interruptExample();
//        yieldExample();
        JoinExample.main(null);
    }


    // Thread.yield(); - меняет состояние потока из running в runnable
    // Прекращает работу и возвращается в исходное состояние отдавая процессорное время другому потоку
    public static void yieldExample() {
        System.out.println("== БЕЗ yield()");
        Thread thread = new Thread(() -> System.out.println("yieldExample(): " + Thread.currentThread().getName()));// выводим имя потока
        thread.start();
        System.out.println("yieldExample(): " + Thread.currentThread().getName());

        System.out.println("== C yield()");
        Thread thread2 = new Thread(() -> System.out.println("yieldExample(): " + Thread.currentThread().getName()));// выводим имя потока
        thread2.start();
        Thread.yield();
        System.out.println("yieldExample(): " + Thread.currentThread().getName());
    }

    /**
     * Вызов метода потока interrupt() просто устанавливает флаг, означающий, что код, использующий этот поток, хочет его прервать.
     * Поток может узнать об этом желании вызовом isInterrupted(), и решить, что ему делать дальше.
     *
     * Часто хорошим решением в этой ситуации является генерация исключения InterruptedException.
     * Посмотрим, например, на поведение Thread.sleep() – приостанавливает поведение потока, но проверяет флаг,
     * и как только замечает, что он установлен, бросает InterruptedException.
     */
    public static void interruptExample() {
        Thread thread = new Thread(() -> System.out.println("interruptExample(): " + Thread.currentThread().getName()));// выводим имя потока
        thread.start();

        thread.interrupt(); // установит флаг interrupt и бросит InterruptedException. Поток пропустит sleep
        try {
            Thread.sleep(3000); // говорим потоку уснуть на 3 секунды
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void sleepExample() {
        Thread thread = new Thread(() -> System.out.println("sleepExample(): " + Thread.currentThread().getName()));// выводим имя потока
        thread.start();
        try {
            Thread.sleep(3000); // говорим потоку уснуть на 3 секунды
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * <p>extends {@link Thread}</p>
     * <p>implement {@link Runnable}</p>
     * <p>implement {@link java.util.concurrent.Callable}</p>
     */
    public static void createNewThreads() throws ExecutionException, InterruptedException {

        // способ 1 extends THREAD
        ThreadExample threadExample = new ThreadExample();
        threadExample.start();

        // способ 2  extends THREAD anonymous Runnable
        new Thread(() -> {
            System.out.println("new Thread(() -> {...}" + Thread.currentThread().getName()); // выводит имя потока
        }).start();

        // способ 3 implement Runnable
        Runnable runnable = new RunnableExample();
        Thread thread = new Thread(runnable);
        thread.start();

        // способ 4 implement Runnable
        Runnable runnable2 = () -> {
            System.out.println("Runnable runnable2 = () -> :" + Thread.currentThread().getName()); // выводит имя потока
        };
        Thread thread2 = new Thread(runnable2);
        thread2.start();

        // способ 5 implement Runnable
        // скобки так же можно опустить если 1 строка в теле
        Runnable runnable3 = () -> System.out.println("Runnable runnable3 = () -> :" + Thread.currentThread().getName()); // выводит имя потока
        Thread thread3 = new Thread(runnable3);
        thread3.start();

        // способ 6 java.util.concurrent.Callable http://pro-java.ru/java-dlya-opytnyx/interfejsy-callable-i-future-v-java/
        ExecutorService pool = Executors.newFixedThreadPool(3);
        String[] arr = {"word1", "word2", "word3"};
        for (String word : arr) {
            Callable<String> callable = new CallableExample(word);
            Future<String> future = pool.submit(callable);
            System.out.println("future.get(): " + future.get());
        }
        System.exit(0);
    }
}

