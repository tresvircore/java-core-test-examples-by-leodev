package MULTITHREADING.multithreading;

/**
 * Пример создания потока с помощью наследования Thread
 */
class ThreadExample extends Thread {
    @Override
    public void run() {
        System.out.println("ThreadExample: " + Thread.currentThread().getName()); // выводит имя потока
    }
}
