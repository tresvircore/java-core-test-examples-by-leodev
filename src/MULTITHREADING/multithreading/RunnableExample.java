package MULTITHREADING.multithreading;

// не возвращает значение
public class RunnableExample implements Runnable {
    @Override
    public void run() {
        System.out.println("RunnableExample: " + Thread.currentThread().getName()); // выводит имя потока
    }
}
