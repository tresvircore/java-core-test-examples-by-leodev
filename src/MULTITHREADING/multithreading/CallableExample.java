package MULTITHREADING.multithreading;

import java.util.concurrent.Callable;

// возвращает значение в отличии от Runnable
public class CallableExample implements Callable<String> {
    private String word;

    public CallableExample(String word) {
        this.word = word;
    }

    @Override
    public String call() {
        System.out.println("CallableExample: " + Thread.currentThread().getName()); // выводит имя потока;
        return Thread.currentThread().getName();
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
