package MULTITHREADING.multithreading;

/**
 * Как синхронизировать метод?
 * Или указать в сигнатуре модификатор synchronized или использовать конструкцию synchronized {} внутри метода.
 */
public class MethodSynchronizationExample {
    public void swap() {
        synchronized (this) { //использовать конструкцию synchronized {} внутри метода
            //someCode
        }
    }

    public synchronized void swap1EqualsSwap() {
        //someCode
    }

    public static synchronized void swap2() { // указать в сигнатуре модификатор synchronized
        //someCode
    }

    public static void swap3EqualsSwap2() {
        synchronized (Object.class) { // блокировщик
            //someCode
        }
    }
}
