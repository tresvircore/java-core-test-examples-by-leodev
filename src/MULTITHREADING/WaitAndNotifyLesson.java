package MULTITHREADING;

/**
 * wait и notify вызываются из synchronized контекста
 * wait(); // говорит пойду посплю, как закончите разбудите меня
 * notify(); // говорит просыпаемся, я закончил работу
 *
 * Для совместной работы  wait и notify они оба должны быть синхронизированы
 * на одном и том же объекте, в нашем случае это ThreadB
 */
 public class WaitAndNotifyLesson {
 public static void main(String[] args) throws InterruptedException {
        ThreadB threadB = new ThreadB();
        threadB.start();
        synchronized (threadB) {
            threadB.wait(); // говорит пойду посплю, как закончите разбудите
        }
        System.out.println(threadB.total);
    }

    static class ThreadB extends Thread {
        int total;

        @Override
        public void run() {
            synchronized (this) {
                for (int i = 0; i < 5; i++) {
                    total += i;
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                notify(); // говорил потоку просыпаемся, я закончил работу
            }
        }
    }
}
