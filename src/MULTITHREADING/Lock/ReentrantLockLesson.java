package MULTITHREADING.Lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock - позволяет лочить несколько методов одновременно.
 * Открыть лок в одном методе и закрыть в другом
 */
public class ReentrantLockLesson {
    public static void main(String[] args) throws InterruptedException {
        RLResource resource = new RLResource();
        resource.i = 5;
        resource.j = 5;
        MyThread myThread = new MyThread();
        myThread.setName("one");

        MyThread myThread1 = new MyThread();
        myThread.resource = resource;
        myThread1.resource = resource;
        myThread.start();
        myThread1.start();
        myThread.join();
        myThread1.join();
        System.out.println(resource.i);
        System.out.println(resource.j);
    }

    static class MyThread extends Thread {
        RLResource resource;

        @Override
        public void run() {
            resource.changeI();
            resource.changeJ();
        }
    }
}

class RLResource { // название класса RL потому что Resource уже используется в другом классе этого пакета
    int i;
    int j;

    Lock lock = new ReentrantLock(); // аналог синхранизации

    void changeI() {
        lock.lock(); // начинает блоккировку
        int i = this.i;
        if (Thread.currentThread().getName().equalsIgnoreCase("one")) {
            Thread.yield();
        }
        i++;
        this.i = i;
//        lock.unlock(); // заканчиваем блокировку
    }

    void changeJ() {
//        lock.lock(); // начинает блоккировку
        int j = this.j;
        if (Thread.currentThread().getName().equalsIgnoreCase("one")) {
            Thread.yield();
        }
        j++;
        this.j = j;
        lock.unlock(); // заканчиваем блокировку
    }
}