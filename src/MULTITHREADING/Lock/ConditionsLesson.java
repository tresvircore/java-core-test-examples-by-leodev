package MULTITHREADING.Lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Condition - аналог wait/notify
 *
 * await: поток ожидает, пока не будет выполнено некоторое условие и пока другой поток не вызовет методы signal/signalAll.
 * Во многом аналогичен методу wait класса Object
 *
 * signal: сигнализирует, что поток, у которого ранее был вызван метод await(),
 * может продолжить работу. Применение аналогично использованию методу notify класса Object
 *
 * signalAll: сигнализирует всем потокам, у которых ранее был вызван метод await(), что они могут продолжить работу.
 * Аналогичен методу notifyAll() класса Object
 */
public class ConditionsLesson {
    static Lock lock = new ReentrantLock();
    static Condition condition = lock.newCondition();
    static int account = 0;

    public static void main(String[] args) {
        AccountPlus accountPlus = new AccountPlus();
        AccountMinus accountMinus = new AccountMinus();
        accountPlus.start();
        accountMinus.start();
    }

    static class AccountPlus extends Thread {
        @Override
        public void run() {
            lock.lock();
            account += 10;
            condition.signal(); //говорит проверять потокам\
            lock.unlock();
        }
    }

    static class AccountMinus extends Thread {
        @Override
        public void run() {
            if(account < 10){
                try {
                    lock.lock();
                    System.out.println("account: " + account);
                    condition.await();
                    System.out.println("account: " + account);
                    lock.unlock();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            account -= 10;
            System.out.println("account at the end: " + account);
        }
    }
}
