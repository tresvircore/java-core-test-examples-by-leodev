package MULTITHREADING;

// volatile говорит не кешируй переменную, а пиши сразу в память.
// Всегда возвращает актуальное значение для всех потоков
public class VolatileLesson {
    volatile static int i;

    public static void main(String[] args) {
        Thread writeThread = new Thread(() -> {
            while (i < 5){
                System.out.println("Increment i to " + (++i));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });

        Thread readThread = new Thread(() -> {
            int localVar = i;
            while (i < 5)
                if (localVar != i) {
                    System.out.println("new value of i is " + i);
                    localVar = i;
                }
        });

        readThread.start();
        writeThread.start();

    }
}
