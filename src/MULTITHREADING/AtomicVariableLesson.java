package MULTITHREADING;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicVariableLesson {
    static int x;
    static AtomicInteger y = new AtomicInteger();

    public static void main(String[] args) throws InterruptedException {
        badUseWithoutAtomic(); // вернет не точное значение, часть значений затеряется например 9_996
        goodUseWithAtomic(); // всегда вернет 10_000
    }

    public static void badUseWithoutAtomic() throws InterruptedException {
        for (int i = 0; i < 10_000; i++)
            new Thread(() -> x++).start(); // x++ НЕ атомарная операция
        Thread.sleep(1_000); // ждем окончания работы потоков
        System.out.println(x);
    }

    public static void goodUseWithAtomic() throws InterruptedException {
        for (int i = 0; i < 10_000; i++)
            new Thread(() -> y.incrementAndGet()).start(); // y.incrementAndGet() атомарная операция
        Thread.sleep(1_000); // ждем окончания работы потоков
        System.out.println(y.get());
    }
}
