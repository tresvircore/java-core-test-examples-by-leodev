package overriding_переопределение;

class Parent{
    synchronized int method(){
        return 1;
    }
}

class Child extends Parent{
    @Override
    final int method() {
        return 2;
    }
}

/**
 * Полиморфный запуск методов, динамическое связывание, полиморфизм
 *  Overriding - переопределение методов
 *  если STATIC или FINAL - не работает перегрузка. Сразу запускает метод, не ищет перегрузку
 *  Можем возвращать CHILD типы иначе должен возвращаться один и тот же тип
 */
public class OverridingLesson {
    public static void main(String[] args) {
        Parent parent = new Parent();
        Parent child = new Child();

        System.out.println(parent.method());
        System.out.println(child.method());
    }


}
