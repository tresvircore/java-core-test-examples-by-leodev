package OOP.inheritance;

import OOP.Car;

public class Toyota extends Car {
    private int numOfSeeds = 2;

    public int getNumOfSeeds() {
        return numOfSeeds;
    }

    public void setNumOfSeeds(int numOfSeeds) {
        this.numOfSeeds = numOfSeeds;
    }

    @Override
    public int getMaxSpeed() {
        return 300;
    }

    @Override
    public String toString() {
        return "Toyota{" +
                "numOfSeeds=" + getNumOfSeeds() +
                "} " + super.toString();
    }
}
