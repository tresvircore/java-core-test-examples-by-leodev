package OOP;

public class Car {
    private int maxSpeed = 100;

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void driving(){
        System.out.println("driving");
    }

    @Override
    public String toString() {
        return "Car{" +
                "maxSpeed=" + getMaxSpeed() +
                '}';
    }
}
