package OOP.encapsulation;

public class Person {
    private String name = "Leo";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
