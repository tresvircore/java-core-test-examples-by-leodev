package OOP.polymorphism;

import OOP.Car;
import OOP.inheritance.Toyota;
// динамическое связывание
public class PolymorphismMain {
    public static void main(String[] args) {
        useCar(new Car());
        useCar(new Toyota());
    }

    private static void useCar(Car car){
        car.driving();
        System.out.println(car.getMaxSpeed());
    }
}
