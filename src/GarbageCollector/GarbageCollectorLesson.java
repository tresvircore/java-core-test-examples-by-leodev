package GarbageCollector;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//https://habrahabr.ru/post/112676/
// java 10 https://habrahabr.ru/post/321856/
public class GarbageCollectorLesson {
    public static void main(String[] args) {
//        method();
//        System.gc();
//        method2();

        gcTest();
    }

    public static void method() {
        Date date;
        date = new Date();
        System.out.println(date);
    }

    public static void method2() {

    }


    /**
     * totalMemory()
     * Возвращает общий объем памяти в виртуальной машине Java. Значение, возвращаемое этим методом, может меняться со временем,
     * в зависимости от среды хоста. Обратите внимание, что объем памяти, необходимый для хранения объекта любого типа, может быть зависимым от реализации.
     *
     * maxMemory()
     * Возвращает максимальный объем памяти, который будет пытаться использовать виртуальная машина Java. Если нет присущего предела,
     * тогда будет возвращено значение Long.MAX_VALUE.
     *
     * freeMemory()
     * Возвращает количество свободной памяти в виртуальной машине Java. Вызов метода gc может привести
     * к увеличению значения, возвращаемого функцией freeMemory.
     *
     * В отношении вашего вопроса maxMemory() возвращает значение -Xmx.
     *
     * Возможно, вам интересно, почему существует totalMemory() И maxMemory(). Ответ заключается в том, что JVM выделяет
     * память лениво. Допустим, вы начали свой Java-процесс как таковой:
     *
     * java -Xms64m -Xmx1024m Foo
     * Ваш процесс начинается с 64 МБ памяти, а если и когда ему нужно больше (до 1024 м), он будет выделять память. totalMemory() соответствует количеству памяти, доступной в настоящее время для JVM для Foo. Если JVM требуется больше памяти, он будет лениво распределять его до максимальной памяти. Если вы используете -Xms1024m -Xmx1024m, значение, которое вы получаете от totalMemory() и maxMemory(), будет равно.
     *
     * Кроме того, если вы хотите точно рассчитать количество использованной памяти, вы делаете это со следующим расчетом:
     *
     * final long usedMem = totalMemory() - freeMemory();
     */
    static void gcTest() {
        DecimalFormat format = new DecimalFormat("###,###,###");
        Runtime runtime = Runtime.getRuntime();
        System.out.println(runtime.maxMemory() / 1000);
        System.out.format("Total memory before     %s Kb \n", format.format((runtime.totalMemory() / 1000)));
        System.out.format("Free memory before      %s Kb \n", format.format((runtime.freeMemory() / 1000)));
        System.out.format("Used memory before      %s Kb \n", format.format((runtime.totalMemory() - runtime.freeMemory() / 1000)));
        List<Date> arrayList = new ArrayList<>(75_000_000);
        for (int i = 0; i < 75_000_000; i++) {
            Date date = new Date();
            arrayList.add(date);
        }

        System.out.format("Total memory after      %s Kb \n", format.format((runtime.totalMemory() / 1000)));
        System.out.format("Free memory after       %s Kb \n", format.format((runtime.freeMemory() / 1000)));
        System.out.format("Used memory after       %s Kb \n", format.format((runtime.totalMemory() - runtime.freeMemory() / 1000)));

        System.gc(); // вызов сборщика не гарантируется
        System.out.format("Total memory after gc   %s Kb \n", format.format((runtime.totalMemory() / 1000)));
        System.out.format("Free  memory after gc   %s Kb \n", format.format((runtime.freeMemory() / 1000)));
        System.out.format("Used memory after gc    %s Kb \n", format.format((runtime.totalMemory() - runtime.freeMemory() / 1000)));
    }
}
