package OOD.L2_Adapter.adapter;

import OOD.L2_Adapter.animation.AnimatedImageViewer;
import OOD.L2_Adapter.animation.GifViewer;
import OOD.L2_Adapter.image.ImageViewer;

public class ImageAdapter implements ImageViewer {

    private AnimatedImageViewer animatedImageViewer;

    public ImageAdapter(String format) {
        if(format.equalsIgnoreCase(".gif"))
            animatedImageViewer = new GifViewer();
    }

    @Override
    public void watchImage(String title, String format) {
        if(format.equalsIgnoreCase(".gif"))
            animatedImageViewer.watchGif(title);
    }
}
