package OOD.L2_Adapter;

import OOD.L2_Adapter.image.PictureViewer;

public class AdapterLessonOOD {
    public static void main(String[] args) {
        PictureViewer viewer = new PictureViewer();
        viewer.watchImage("space", ".png");
        viewer.watchImage("admin", ".jpeg");
        viewer.watchImage("square", ".bmp");
        viewer.watchImage("animation", ".gif");
        viewer.watchImage("image", ".jpg");
    }
}
