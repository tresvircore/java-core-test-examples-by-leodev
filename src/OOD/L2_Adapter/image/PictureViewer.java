package OOD.L2_Adapter.image;

import OOD.L2_Adapter.adapter.ImageAdapter;

public class PictureViewer implements ImageViewer {
    @Override
    public void watchImage(String title, String format) {
        switch (format) {
            case ".png":
                System.out.println("Watching " + title + ".png");
                break;
            case ".jpeg":
                System.out.println("Watching " + title + ".jpeg");
                break;
            case ".bmp":
                System.out.println("Watching " + title + ".bmp");
                break;
            case ".gif":
                ImageAdapter imageAdapter = new ImageAdapter(format);
                imageAdapter.watchImage(title, format);
                break;
            default:
                System.out.println("Format  " + format + " not supported");
                break;
        }
    }
}
