package OOD.L2_Adapter.image;

public interface ImageViewer {
    void watchImage(String title, String format);
}
