package OOD.L1_AbstractFactory.nike;

public class NikeShoes implements NikeProduct {

    @Override
    public void makeNikeProduct() {
        System.out.println("Make Nike Shoes");
    }
}
