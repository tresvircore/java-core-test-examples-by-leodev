package OOD.L1_AbstractFactory.nike;

public interface NikeProduct {
    void makeNikeProduct();
}
