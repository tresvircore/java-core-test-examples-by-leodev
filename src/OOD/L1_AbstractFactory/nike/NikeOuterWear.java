package OOD.L1_AbstractFactory.nike;

public class NikeOuterWear implements NikeProduct {
    @Override
    public void makeNikeProduct() {
        System.out.println("Make Adidas OuterWear");
    }
}
