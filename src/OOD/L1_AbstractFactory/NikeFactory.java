package OOD.L1_AbstractFactory;

import OOD.L1_AbstractFactory.adidas.AdidasProduct;
import OOD.L1_AbstractFactory.nike.NikeOuterWear;
import OOD.L1_AbstractFactory.nike.NikeProduct;
import OOD.L1_AbstractFactory.nike.NikeShoes;

public class NikeFactory extends AbstractFactory {
    @Override
    AdidasProduct getAdidasProduct(String product) {
        return null;
    }

    @Override
    NikeProduct getNikeProduct(String product) {
        if (product == null)
            return null;
        if (product.equalsIgnoreCase("NIKE OUTERWEAR"))
            return new NikeOuterWear();
        if (product.equalsIgnoreCase("NIKE SHOES"))
            return new NikeShoes();
        return null;
    }
}
