package OOD.L1_AbstractFactory.adidas;

import OOD.L1_AbstractFactory.adidas.AdidasProduct;

public class AdidasOuterWear implements AdidasProduct {
    @Override
    public void makeAdidasProduct() {
        System.out.println("Make Adidas Outerwear");
    }
}
