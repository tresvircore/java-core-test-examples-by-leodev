package OOD.L1_AbstractFactory.adidas;

public class AdidasShoes implements AdidasProduct {
    @Override
    public void makeAdidasProduct() {
        System.out.println("Make Adidas Shoes");
    }
}
