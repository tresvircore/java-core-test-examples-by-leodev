package OOD.L1_AbstractFactory;

import OOD.L1_AbstractFactory.adidas.AdidasProduct;
import OOD.L1_AbstractFactory.nike.NikeProduct;

/**
 * <a href="https://youtu.be/F9Gyl7AaPBU?list=PLGbj5Xe61j2B6lAwuXKXMAH9xfmHd5fBD">Java OOD. Урок 1. Шаблон Abstract Factory</a>
 * ФАБРИКА ФАБРИК
  */

public class AbstractFactoryLessonOOD {
    public static void main(String[] args) {
        AbstractFactory adidasFactory = FactoryGenerator.getFactory("Adidas");
        AdidasProduct adidasProduct = adidasFactory.getAdidasProduct("Adidas shoes");
        adidasProduct.makeAdidasProduct();

        AbstractFactory nikeFactory = FactoryGenerator.getFactory("Nike");
        NikeProduct nikeProduct = nikeFactory.getNikeProduct("Nike outerwear");
        nikeProduct.makeNikeProduct();
    }
}
