package OOD.L1_AbstractFactory;

import OOD.L1_AbstractFactory.adidas.AdidasOuterWear;
import OOD.L1_AbstractFactory.adidas.AdidasProduct;
import OOD.L1_AbstractFactory.adidas.AdidasShoes;
import OOD.L1_AbstractFactory.nike.NikeProduct;

public class AdidasFactory extends AbstractFactory {
    @Override
    AdidasProduct getAdidasProduct(String product) {
        if(product == null)
            return null;
        if(product.equalsIgnoreCase("ADIDAS OUTERWEAR"))
            return new AdidasOuterWear();
        if(product.equalsIgnoreCase("ADIDAS SHOES"))
            return new AdidasShoes();
        return null;
    }

    @Override
    NikeProduct getNikeProduct(String product) {
        return null;
    }
}
