package OOD.L1_AbstractFactory;

import OOD.L1_AbstractFactory.adidas.AdidasProduct;
import OOD.L1_AbstractFactory.nike.NikeProduct;

public abstract class AbstractFactory {

    abstract AdidasProduct getAdidasProduct(String product);

    abstract NikeProduct getNikeProduct(String product);
}
