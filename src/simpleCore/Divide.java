package simpleCore;

public class Divide {
    /**
     * деление без деления
     * @param a делимое
     * @param b делитель
     * @return результат деления
     */
    public static int divide(int a, int b) {
        if (a == b) return 1;
        if (a < b) return 0;
        int d = 1;
        int c = a - b;
        while (c > 0) {
            c -= b;
            d++;
        }
        return d;
    }

    /**
     * деление без деления рефлексией
     * @param a делимое
     * @param b делитель
     * @return результат деления
     */
    public static int div(int a, int b) {
        if (a - b > 0) {
            return 1 + div(a - b, b);
        } else if (a == b) {
            return 1;
        } else {
            return 0;
        }
    }
}
