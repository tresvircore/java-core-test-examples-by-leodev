package java9;

import java.io.IOException;
import java.util.Optional;

public class ProcessControlLesson {
    public static void main(String[] args) throws IOException, InterruptedException {
        Process process = Runtime.getRuntime().exec("ping ya.ru -t");
        System.out.println("current process id " + ProcessHandle.current().pid());
        System.out.println("current process id " + process.pid());

        ProcessHandle ph = ProcessHandle.of(process.pid()).orElseThrow(IllegalAccessError::new);
        ph.onExit().thenRun(() -> System.out.println("process has been closed"));
        System.out.println(ph.info().user().orElse("No user"));
        System.out.println(ph.info().commandLine().orElse("empty"));

        ph.destroy();

        Thread.sleep(100);
    }
}
