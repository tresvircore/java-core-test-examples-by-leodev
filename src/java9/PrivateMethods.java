package java9;

public class PrivateMethods {
    public static void main(String[] args) {
        A a = new A(){};
        a.print("Hello World");
    }
}

interface A{
    private void privatePrint(String s){
        System.out.println(s);
    }

    default void print(String s) {
         privatePrint(s);
    }
}