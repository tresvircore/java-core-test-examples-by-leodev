package java9;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ImmutableCollectionsLesson {
    public static void main(String[] args) {
        List<Integer> integerList = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(integerList);

//        Set<Integer> integerSet = Set.of(1,2,3,4,4); // Exception
        Set<Integer> integerSet = Set.of(1,2,3,4); // Exception
        System.out.println(integerSet);

        Map<String, String> map = Map.of("key1", "val_1", "key2", "val_2");
        System.out.println(map.get("key1"));

        Map<String, String> map2 = Map.ofEntries(
                Map.entry("key3", "val_3"),
                Map.entry("key4", "val_4")
        );
        System.out.println(map2);
    }
}
