package java9;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Стрим все равно будет закрыт после выхода из try
 */
public class TryBlockLesson {
    public static void main(String[] args) throws FileNotFoundException {
        OutputStream outputStream = new FileOutputStream("temp.txt");
        try (outputStream) {
            outputStream.write("hello".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
