package reflection;

import java.lang.reflect.*;

/**
 * Reflection Lesson
 * https://youtu.be/7eYQPNNxLSM?list=PL786bPIlqEjRDXpAKYbzpdTaOYsWyjtCX
 */
public class ReflectionLesson {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
//        Class clazz = getClassType();
//        getClassInfoByReflection(clazz);
        changeClassInfoByReflection();
        callMethodWithParamByReflection();
    }

    private static void callMethodWithParamByReflection() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SomeClass someClass = new SomeClass();
        Class clazz = someClass.getClass();
        Method someMethod = clazz.getDeclaredMethod("someMethod", String.class); // достаем метод по имени и типу параметра
        someMethod.invoke(someClass, "text of parameter"); // вызываем метод у объекта "someClass" и передает строку как параметр
    }

    private static void changeClassInfoByReflection() throws NoSuchFieldException, IllegalAccessException {
        SomeClass someClass = new SomeClass();
        Class clazz = someClass.getClass();

        Field someVar = clazz.getDeclaredField("someVar");
        System.out.println(someVar.get(someClass)); // выводим значение
        someVar.setAccessible(true); // разрешаем изменять
        someVar.set(someClass, "HELLO REFLECTION"); // изменяем
        System.out.println(someVar.get(someClass)); // выводим значение
    }

    public static void getClassInfoByReflection(Class clazz) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        SomeClass someClass = (SomeClass) clazz.getDeclaredConstructor().newInstance();

        System.out.println("clazz.getName(): " + clazz.getName());
        System.out.println("\n=== CONSTRUCTORS:");
        Constructor[] declaredConstructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : declaredConstructors) {
            System.out.println("constructor: " + constructor);
            System.out.println("constructor.getName(): " + constructor.getName());

            System.out.println("\n=== PARAMETERS:");
            Parameter[] parameters = constructor.getParameters();
            for (Parameter parameter : parameters) {
                System.out.println("parameter: " + parameter);
                System.out.println("parameter.getName(): " + parameter.getName());
                System.out.println("parameter.getType(): " + parameter.getType());
                System.out.println("parameter.getType().getName(): " + parameter.getType().getName());
            }

        }

        System.out.println("\n====== METHODS ======");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("method: " + method);
            System.out.println("method.getName(): " + method.getName());

            System.out.println("\n=== METHOD PARAMETERS:");
            Parameter[] methodParameters = method.getParameters();
            for (Parameter methodParameter : methodParameters) {
                System.out.println("methodParameter: " + methodParameter);
                System.out.println("methodParameter.getName(): " + methodParameter.getName());
                System.out.println("methodParameter.getType(): " + methodParameter.getType());
                System.out.println("methodParameter.getType().getName(): " + methodParameter.getType().getName());
            }
            System.out.println("\n=== METHOD MODIFIERS:");
            System.out.println(Modifier.toString(method.getModifiers()));
            System.out.println("method.getReturnType(): " + method.getReturnType());
            System.out.println("method.getReturnType().getName(): " + method.getReturnType().getName());

            System.out.println("\n=== FIELDS:");
            Field[] declaredFields = clazz.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                System.out.println("declaredField: " + declaredField);
                System.out.println("declaredField.getName(): " + declaredField.getName());
                System.out.println("declaredField.getType(): " + declaredField.getType());
                System.out.println("declaredField.getModifiers(): " + Modifier.toString(declaredField.getModifiers()));
            }

        }
    }

    // есть 3 способа получения объекта Class
    public static Class getClassType() throws ClassNotFoundException {
        SomeClass someClass = new SomeClass();
        Class clss = someClass.getClass();
        Class clss2 = SomeClass.class;
        Class clss3 = Class.forName("reflection.SomeClass");
        return clss;
    }
}


class SomeClass {
    private static transient int i;
    String someVar = "Test Var";

    public SomeClass() {
    }

    public SomeClass(String someVar) {
        this.someVar = someVar;
    }

    public String someMethod(String s) {
        System.out.println("This is " + s);
        return s;
    }
}
