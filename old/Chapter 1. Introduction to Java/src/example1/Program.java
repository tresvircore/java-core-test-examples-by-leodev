package example1;// подключение используемых в программе внешних пакетов

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Данный пример работает только если JVM запускалась вручную через консоль, иначе у автоматически
 * запущенной машины System.console() будет возвращать NULL.
 * Альтернативным решением является реализация AlternativeProgram c помощью BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 */
/* объявление нового класса */
public class Program {

    public static void main(String args[]) { /* объявление нового метода */

        String name; // переменная для имени
        Console con = System.console(); // получаем объект консоли для считывания с консоли
        name = con.readLine("Введите свое имя: "); // считываем введенное значение
        System.out.println("Добро пожаловать, " + name);
    } /* конец объявления нового метода */
}/* конец объявления нового класса*/

class AlternativeProgram {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter String: ");
        String s = br.readLine();
        System.out.println(s);
        try {
            System.out.print("Enter Integer: ");
            int i = Integer.parseInt(br.readLine());
            System.out.println(i);
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid Format!");
        }
    }

}