package ru.leodev.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
    static final String text = "Тайланд красивая страна. Многие люди посещают Таиланд круглый год";

    static boolean test(String s){
        Pattern p = Pattern.compile("^Ivan$"); // здали правило
        Matcher m = p.matcher(s); // применили правило к формальному параметру
        return m.matches(); // вернули результат соответствия строки с правилом
    }

    /**
     * ========== МЕТАСИМВОЛЫ ============
     * ^ (циркумфлекс) - начало проверяемой строки
     * $ (доллар) - конец проверяемой строки
     * . (точка) - любой символ
     * | - ИЛИ
     * ? - предшествующий вопросу символ не обязательный
     * + - любое колличество символов
     * * - любое кол-во экземпляров элемента(в том числе нулевое)
     * \\d - цифровой сивол
     * \\D - НЕ цифровой сивол
     * \\s - пробельный символ
     * \\S - НЕ пробельный символ
     * \\w - буквенный или цифровой символ или знак подчеркивания
     * \\W - любой символ кроме буквенного или цифрового или знака подчеркивания
     *
     * ========== КВАНТИФИКАТОРЫ ============
     * + - одно или более
     */
    static boolean test2(String s){
        Pattern p = Pattern.compile(".+\\.(com|ru|org)");
        Matcher m = p.matcher(s);
        return m.matches();
    }
    public static void main(String[] args) {
//        System.out.println(test("Ivan"));
//        System.out.println(test(" Ivan"));
//        System.out.println(test("ivan"));

//        System.out.println(text.replaceAll("[Тт]а[ий]ланд", "Россия"));

        System.out.println(test2("site.ru")); //true
        System.out.println(test2("site.ua")); //false
        System.out.println(test2("site.ru.ua")); //false
    }
}
