package ru.leodev.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Demo {
    private Pattern pattern, pattern2;
    private Matcher m;
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
    private static final String IP_PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public Demo() {
        pattern = Pattern.compile(EMAIL_PATTERN);
        pattern2 = Pattern.compile(IP_PATTERN);
    }

    boolean checkIP(String ip){
        m = pattern2.matcher(ip);
        return m.matches();
    }

    boolean checkEmail(String email){
        m = pattern.matcher(email);
        return m.matches();
    }

    public static void main(String[] args) {
        Demo d = new Demo();
        System.out.println("email: " + d.checkEmail("info@test.ru")); // true
        System.out.println("email: " + d.checkEmail("info@te_st.ru")); // false
        System.out.println("email: " + d.checkEmail("info-test.ru")); // false

        System.out.println("IP: " + d.checkIP("10.115.15.100"));
        System.out.println("IP: " + d.checkIP("192.168.1.4"));
    }
}
