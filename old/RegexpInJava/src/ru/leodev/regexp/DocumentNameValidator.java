package ru.leodev.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DocumentNameValidator {
    Pattern p, pWithNumber;
    Matcher m, mWithNumber;

    public DocumentNameValidator() {
        p = Pattern.compile("(Приход|Оплата|Продажа|Накладная|Накладная \\(возврат\\))\\s+([№(])([А-Яа-яa-zA-Z0\"-]+)?([a-zA-Zа-яА-Я0-9-]+)([/0-9\\\\]+)?\\sот.+");
//        p = Pattern.compile("(.+)\\s+([№(])([А-Яа-яa-zA-Z0\"-]+)?([a-zA-Zа-яА-Я0-9-]+)([/0-9\\\\]+)?\\sот.+");
        pWithNumber = Pattern.compile("(\\D)?(0+)?(\\d+)");
    }

    //ТСС-000442, СЭ000002312, 173, 4078, 173, 86, МДКМ-184, 63
    public String getDocumentName(String docName) {
        if (docName.contains("№")) {
            return getDocumentWithNumberSign(docName);
        } else {
            m = p.matcher(docName);
            if (m.find()) {
                return m.group(4);
            }
            return null;
        }
    }

    /**
     * remove characters before '№', clean str, then match
     * @param docName
     * @return
     */
    // 4467, 551, 86, 63
    private String getDocumentWithNumberSign(String docName) {
        String s;
        int index = docName.indexOf("№");
        s = docName.substring(index);
        s = s.split(" ")[0];
        mWithNumber = pWithNumber.matcher(s);
        if (mWithNumber.find()) {
            s = mWithNumber.group(3);
            try {
                Integer.parseInt(s);
            } catch (NumberFormatException e) {
                return null;
            }
        }
        return s;
    }
}

/*

18 094/5
    */
