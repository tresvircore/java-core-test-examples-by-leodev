package accessModifiersAndEncapsulation.encapsulation.package2;

import accessModifiersAndEncapsulation.encapsulation.package1.ClassA;

public class ClassC{

    public void result(){
        ClassA classA = new ClassA();
        //System.out.println(classA.num1); // ошибка, так как num1 - private
        classA.displayNum1(); // public

        //System.out.println(classA.num2); // ошибка, так как num2 - идентификатор по умолчанию
        //classA.displayNum2(); //ошибка, так как доступ - protected

        //System.out.println(classA.num3); // ошибка, так как доступ - protected
        //classA.displayNum3(); //ошибка, так как доступ - идентификатор по умолчанию

        System.out.println(classA.num4); // public
        //classA.displayNum4(); // ошибка, так как displayNum4() - private
    }
}