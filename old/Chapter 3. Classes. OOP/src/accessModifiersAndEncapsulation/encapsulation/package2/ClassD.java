package accessModifiersAndEncapsulation.encapsulation.package2;

import accessModifiersAndEncapsulation.encapsulation.package1.ClassA;


public class ClassD extends ClassA {

    public void result() {
        //System.out.println(num1); // ошибка, так как num1 - private
        displayNum1(); // public

        //System.out.println(num2); // ошибка, так как доступ - идентификатор по умолчанию
        displayNum2(); // protected

        System.out.println(num3); // protected
        //displayNum3();  //ошибка, так как доступ по умолчанию

        System.out.println(num4); // public
        //classA.displayNum4(); // ошибка, так как displayNum4() - private
    }
}