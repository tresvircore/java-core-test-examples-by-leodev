package p2_Creating_a_Data_Stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class CreatingADataStreamApp {
    public static void main(String[] args) {
        createStreamFromList();
        createStreamFromArray();
        createStream_WithVariables();
        createStream_return_Exception();
        createStreamFromPrimitives();
        createStreamUse_OF_method();
    }

    /**
     * Для создания потока данных можно применять различные методы.
     * В качестве источника потока мы можем использовать коллекции. В частности, в JDK 8 в интерфейс Collection,
     * который реализуется всеми классами коллекций, были добавлены два метода для работы с потоками:
     * default Stream<E> stream: возвращается поток данных из коллекции
     * default Stream<E> parallelStream: возвращается параллельный поток данных из коллекции
     * Так, рассмотрим пример с ArrayList:
     */
    private static void createStreamFromList() {
        List<String> cities = new ArrayList<>();
        cities.addAll(Arrays.asList("Париж", "Лондон", "Мадрид"));
        cities.stream() // получаем поток
                .filter(s -> s.length() == 6) // применяем фильтрацию по длине строки
                .forEach(s -> System.out.println(s)); // выводим отфильтрованные строки на консоль
    }

    private static void createStreamFromArray() {
        Stream<String> citiesStream = Arrays.stream(new String[]{"Париж", "Лондон", "Мадрид"}) ;
        citiesStream.forEach(System.out::println); // выводим все элементы массива

    }

    /**
     * Здесь с помощью вызова cities.stream() получаем поток, который использует данные из списка cities.
     * С помощью каждой промежуточной операции, которая применяется к потоку, мы также можем получить поток с учетом модификаций.
     * Например, мы можем изменить предыдущий пример следующим образом:
     */
    private static void createStream_WithVariables() {
        ArrayList<String> cities = new ArrayList<>();
        cities.addAll(Arrays.asList("Париж", "Лондон", "Мадрид"));
        Stream<String> citiesStream = cities.stream(); // получаем поток
        citiesStream = citiesStream.filter(s -> s.length() == 6); // применяем фильтрацию по длине строки
        citiesStream.forEach(s -> System.out.println(s)); // выводим отфильтрованные строки на консоль
    }

    /**
     * Важно, что после использования терминальных операций другие терминальные или промежуточные операции к этому же
     * потоку не могут быть применены, поток уже употреблен. Например, в следующем случае мы получим ошибку:<br/>
     *
     * <B>Фактически жизненный цикл потока проходит следующие три стадии: <B/><br/><br/>
     * - Создание потока<br/>
     * - Применение к потоку ряда промежуточных операций<br/>
     * - Применение к потоку терминальной операции и получение результата
     */
    private static void createStream_return_Exception() {
        ArrayList<String> cities = new ArrayList<>();
        cities.addAll(Arrays.asList("Париж", "Лондон", "Мадрид"));
        Stream<String> citiesStream = cities.stream(); // получаем поток
        citiesStream = citiesStream.filter(s -> s.length() == 6); // применяем фильтрацию по длине строки
        citiesStream.forEach(System.out::println); // выводим отфильтрованные строки на консоль

        long number = citiesStream.count(); // здесь ошибка, так как поток уже употреблен
        System.out.println(number);
        citiesStream = citiesStream.filter(s->s.length()>5); // тоже нельзя, так как поток уже употреблен
    }

    /**
     * Для создания потоков IntStream, DoubleStream, LongStream можно использовать соответствующие перегруженные версии этого метода:
     */
    public static void createStreamFromPrimitives() {
        IntStream intStream = Arrays.stream(new int[]{1,2,4,5,7});
        intStream.forEach(i->System.out.println(i));

        LongStream longStream = Arrays.stream(new long[]{100,250,400,5843787,237});
        longStream.forEach(l->System.out.println(l));

        DoubleStream doubleStream = Arrays.stream(new double[] {3.4, 6.7, 9.5, 8.2345, 121});
        doubleStream.forEach(d->System.out.println(d));
    }

    /**
     * И еще один способ создания потока представляет статический метод of(T..values) класса Stream:
     */
    public static void createStreamUse_OF_method() {
        Stream<String> citiesStream =Stream.of("Париж", "Лондон", "Мадрид");
        citiesStream.forEach(s->System.out.println(s));

        IntStream intStream = IntStream.of(1,2,4,5,7);
        intStream.forEach(i->System.out.println(i));

        LongStream longStream = LongStream.of(100,250,400,5843787,237);
        longStream.forEach(l->System.out.println(l));

        DoubleStream doubleStream = DoubleStream.of(3.4, 6.7, 9.5, 8.2345, 121);
        doubleStream.forEach(d->System.out.println(d));
    }

}
