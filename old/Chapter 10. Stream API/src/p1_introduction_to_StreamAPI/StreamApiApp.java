package p1_introduction_to_StreamAPI;

import java.util.stream.IntStream;

public class StreamApiApp {
    private static int[] numbers = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};

    public static void main(String[] args) {
        findNumbersAboveZeroOldMethod(numbers);
        findNumbersAboveZeroStreamApiMethod(numbers);
    }

    /**
     * Допустим, у нас есть задача: найти в массиве количество всех чисел, которые больше 0.
     * До JDK 8 мы бы могли написать что-то наподобие следующего:
     */
    public static void findNumbersAboveZeroOldMethod(int[] arr) {
        int count = 0;
        for (int i : numbers) {
            if (i > 0) count++;
        }
        System.out.println(count);
    }

    public static void findNumbersAboveZeroStreamApiMethod(int[] arr) {
        long count = IntStream.of(arr).filter(w -> w > 0).count();
        System.out.println(count);
    }
}
