package java8;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import java.util.List;

public class StreamParallelismLessonTest {
    private List<Integer> integers;

    @BeforeEach
    void setUp() {
        integers = StreamParallelismLesson.populateListIntegers();
    }

    @RepeatedTest(10)
    void checkParallelStreamVSStream() {
        StreamParallelismLesson.checkParallelStreamVSStream(integers);
    }
}
